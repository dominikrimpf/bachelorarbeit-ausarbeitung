\chapter{Umsetzung}\label{chap:umsetzung}
\section{Programmierung}\label{sec:programmierung}

Die Simulation der Glasfaser und alle zusätzlichen Funktionen sind vollständig in der Programmiersprache Python \cite{python3} implementiert. Hierfür wird vor allem das Framework NumPy \cite{NumPy} für die mathematischen Operationen und Matplotlib \cite{matplotlib} zum Darstellen der Ergebnisse verwendet. Als \ac{IDE} kommt JupyterLab \cite{jupyterlab}, das im Browser läuft, zum Einsatz. Vorteil dieser \ac{IDE} ist, dass die Ausgabewerte und Grafen direkt eingebettet angezeigt werden. Zur einfacheren Verwendung der implementierten Funktionen werden diese auch direkt in Python-Dateien exportiert. Der Code ist unter \url{https://github.com/domrim/bachelorarbeit-code} verfügbar.

\subsection{Vorbereitungen}\label{subsec:vorbereitung}

Zur späteren Validierung der Implementierung des Kanals werden Testsignale benötigt. Um diese zu generieren sind mehrere Funktionen implementiert. Hierbei wurden einige Funktionen aus den Vorlesungsbeispielen Nachrichtentechnik 1 \cite{CEL2019} modifiziert und wiederverwendet.

\subsubsection{Pulsformen}

Für die Modulation der Daten werden mehrere Pulsformungen implementiert. Es werden insgesamt drei Filter implementiert: Gauss-Filter (\code{get\_gauss\_ir}), \ac{RC}-Filter (\code{get\_rc\_ir}) und \ac{RRC}-Filter (\code{get\_rrc\_ir}).\\

Das Gauss-Filter besitzt nach \cite{Kammeyer2018} die Impulsantwort
\begin{equation}
h(t) = \frac{\alpha}{\sqrt{\pi}}\exp-(\alpha t)^2 \eqdot
\end{equation}
Die Flankensteilheit der Impulsantwort des Gauß-Filters wird nicht direkt über den Wert $\alpha$ gesteuert, sondern über das Argument \code{energy\_factor} ($f_e$). Dieses gibt, wie dargestellt in \cref{fig:param_gauss}, an welcher Anteil der Gesamtenergie der Impulsantwort zwischen $- T_\text{symbol} / 2$ und $T_\text{symbol} / 2$ liegen soll. Für die Bestimmung von $\alpha$ aus dem Energie-Faktor $f_e$ wird folgender Ansatz gemacht
\begin{equation}
f_e \cdot \int_{-\infty}^{\infty}  \left|\frac{\alpha}{\sqrt{\pi}}\exp-(\alpha t)^2\right|^2 \text{d}t = \int_{-\frac{T_\text{symbol}}{2}}^{\frac{T_\text{symbol}}{2}}  \left|\frac{\alpha}{\sqrt{\pi}}\exp-(\alpha t)^2\right|^2 \text{d}t \eqdot
\label{eq:energy_factor}
\end{equation}
Die Integration der e-Funktion kann mit $\text{erf}(x) = \frac{2}{\sqrt{\pi}} \int_{0}^{x}\exp(-t^2)\text{d}t$ aus \cite{Bronstein2016} gelöst werden. Die Lösung für \cref{eq:energy_factor}
\begin{equation}
f_e \cdot \left|\frac{\alpha}{\sqrt{\pi}}\right|^2 \cdot \sqrt{\frac{\pi}{2}} \cdot \frac{1}{\alpha} = \left|\frac{\alpha}{\sqrt{\pi}}\right|^2 \cdot \sqrt{\frac{\pi}{2}} \cdot \frac{1}{\alpha} \cdot \text{erf}\left(\frac{T\alpha}{\sqrt{2}}\right)
\end{equation}
lässt sich dann zu
\begin{equation}
\alpha = \frac{\sqrt{2}\cdot\text{erf}^{-1}\left(f_e\right)}{T_\text{symbol}}
\end{equation}
vereinfachen. Diese Gleichung wird für die Umrechnung von dem Argument \code{energy\_factor} den Parameter $\alpha$ für die Gauss-Funktion genutzt.\\
\begin{figure}[htb]
    \centering
    \input{figures/param_gauss.tex}
    \caption{Darstellung Gauss-Parameter $f_e$}
    \label{fig:param_gauss}
\end{figure}

Die Impulsantwort eines \ac{RC}-Filters ist nach \cite{Kammeyer2018} wie folgt definiert:
\begin{equation}
h(t) = \frac{1}{T}\frac{\sin\left(\pi \frac{t}{T}\right)}{\pi \frac{t}{T}}\frac{\cos\left(\pi r\frac{t}{T}\right)}{1-\left(2r \frac{t}{T}\right)^2} \eqdot
\label{eq:raised-cosine-ir}
\end{equation}
Der Parameter $r$ ist der sogenannte Roll-Off-Faktor, der die Flankensteilheit des Filters steuert. Wie in \cref{eq:raised-cosine-ir} zu sehen ist, hat die Funktion an den Stellen $t = 0$ und $|t| = \frac{T}{2r},\ r \neq 0$ Grenzwerte, bei denen Zähler und Nenner der Funktion gegen 0 konvergieren. In diesem Fall lassen sich die Grenzwerte sich mit der Regel von \textit{de l'Hospital} berechnen. Für $t = 0$ ergibt sich
\begin{equation}
h(t) = \frac{1}{T}
\end{equation}
und für $|t| = \frac{T}{2r}, r \neq 0$ ergibt sich
\begin{equation}
h(t) = \frac{sin\left(\pi \frac{t}{T}\right)}{\pi t}\frac{cos\left(\pi r\frac{t}{T}\right)}{1 - \left(2r \frac{t}{T}\right)^2} \eqdot
\end{equation}\\


Für das \ac{RRC}-Filter gilt nach \cite{Kammeyer2018}
\begin{equation}
h(t) = \frac{4r\frac{t}{T}\cos\left(\pi(1+r)\frac{t}{T}\right)+\sin\left(\pi\left(1-r\right)\frac{t}{T}\right)}{\left(1-\left(4r\frac{t}{T}\right)^2\right)\pi t} \eqdot
\label{eq:root-raised-cosine-ir}
\end{equation}
Auch hier ist $r$ der Roll-Off-Faktor. Diese Impulsantwort hat, ähnlich wie \cref{eq:raised-cosine-ir}, Grenzwerte bei $t=0$ und $|t|=\frac{T}{4r},\ r \neq 0$. An diesen Stellen konvergieren Nenner und Zähler gegen 0, daher kann  auch hier die Regel von \textit{de l'Hospital} angewendet werden. Für den Grenzwert $t=0$ ergibt sich
\begin{equation}
h(t) = \frac{\pi + 4r - \pi r}{\pi T}
\end{equation}
als Lösung für \cref{eq:root-raised-cosine-ir}. Für den Grenzwert an den Stellen $|t|=\frac{T}{4r},\ r \neq 0$ ergibt sich die Lösung
\begin{equation}
h(t) = \frac{r\left(-2\cos\left(\frac{\pi\left(1+r\right)}{4r}\right)+\pi\sin\left(\frac{\pi\left(1+r\right)}{4r}\right)\right)}{\pi T} \eqdot
\end{equation}\\

Alle Impulsantworten werden zur einfacheren Weiterverwendung auf die Energie \SI{1}{\joule} normiert. Die Energie eines Signals lautet wie in \cref{subsec:energysignal} beschrieben
\begin{equation}
E \approx T_{\text{sample}}\cdot\sum_{k=-\infty}^\infty \cdot |g(kT_{\text{sample}})|^2
\end{equation}
Um nun die Energie auf den Wert $E = 1$ zu normieren muss
\begin{equation}
E \approx T_{\text{sample}}\cdot\sum_{k=-\infty}^\infty \cdot |g(kT_{\text{sample}})|^2 \overset{!}{=} 1
\end{equation}
werden. Dafür wird die Energie des nicht normierten Signals benötigt. Diese wird dann zur Skalierung des Signals verwendet. Da die Energie mit dem Betragsquadrat der einzelnen Werte des Signals zusammen hängt müssen die Werte mit der Wurzel der derzeitigen Energie des Signals skaliert werden. Das normierte Signal $g_{\text{normiert}}(kT_{\text{sample}})$ ist dann
\begin{equation}
g_{\text{normiert}}(kT_{\text{sample}}) = \frac{g(kT_{\text{sample}})}{\sqrt{T_{\text{sample}}\cdot\sum_{k=-\infty}^\infty \cdot |g(kT_{\text{sample}})|^2}} \eqdot
\end{equation}
Mit dem Zusammenhang $||g(kT_{\text{sample}})|| = \sqrt{\sum_{k=-\infty}^\infty \cdot |g(kT_{\text{sample}})|^2}$ aus \cite{Puente2019} kann die Gleichung nun noch zu
\begin{equation}
g_{\text{normiert}}(kT_{\text{sample}}) = \frac{g(kT_{\text{sample}})}{\sqrt{T_{\text{sample}}}\cdot||g(kT_{\text{sample}})||}
\label{eq:enormierung}
\end{equation}
vereinfacht werden.\\

Die \ac{RC}- und \ac{RRC}-Filter erfüllen die erste Nyquistbedingung \cite{Kammeyer2018}. Die Anzahl der Nullstellen der Impulsantworten wird über den Parameter \code{syms} $n_{syms}$ gesteuert. Der Parameter definiert, wie viele Nullstellen sich jeweils links beziehungsweise rechts vom globalen Maximum der Impulsantwort befinden. Das bedeutet, dass eine Impulsantwort insgesamt $2\cdot n_\text{syms}$ Nullstellen hat. Für alle Filter definiert der Parameter \code{syms} $n_{syms}$ zusammen mit \code{f\_symbol} $f_\text{symbol}$ die Dauer der Impulsantwort $T_\text{ir}$ über den Zusammenhang
\begin{equation}
T_\text{ir} = 2\cdot\frac{1}{f_\text{symbol}}\cdot n_\text{syms} \eqdot
\end{equation}
Die Impulsantworten der Filter die die erste Nyquistbedingung erfüllen können dann mit dem zeitlichen Abstand $T_\text{symbol} = 1 / f_\text{symbol}$ überlagert werden ohne das \ac{ISI} auftritt.\\

Für die Funktionen des \ac{RC}- und des \ac{RRC}-Filters ist für die Impulsantwort eine Fallunterscheidung für die Grenzwerte implementiert. Die Funktionen zur Berechnung der Impulsantwort besitzen gemeinsame Parameter, die in \cref{tab:parameters_puls} beschrieben sind.
\begin{table}[htb]
  \centering
  \begin{tabular}{ >{\ttfamily\selectfont}l >{\ttfamily\selectfont}l l}
    \hline
    \textrm{Parameter} & \textrm{Datentyp} & Beschreibung \\
    \hline
    syms & int & Normierte Anzahl an Symbolen pro Impulsantwort.\\
    f\_symbol & float & Symbolrate\\
    n\_up & int & Überabtastung-Faktor\\
    \hdashline
    \textrm{RC und RRC:}\\
    r & float & Roll-Off-Faktor\\
    \hdashline
    \textrm{Gauß:}\\
    energy\_factor & float & Anteil der Gesamtenergie zwischen $- T_\text{symbol} / 2$ und $T_\text{symbol} / 2$\\
    \hline
  \end{tabular}
  \caption{Parameter für alle Impulsantwort-Funktionen}
  \label{tab:parameters_puls}
\end{table}

Die Filter-Funktionen geben zwei Werte zurück. Als erstes wird die Impulsantwort des Filters in einem Array (\code{[...]}) zurück gegeben, der zweite Wert ist ein Float und enthält die Dauer zwischen zwei Abtastwerten $T_\text{sample}$.
In \cref{fig:fir} sind die Impulsantworten aller drei Filter zu sehen. Die Parameter mit der die Funktionen aufgerufen wurden sind in \cref{tab:fir_params} zu sehen.
\begin{figure}[htb]
	\centering
	\input{figures/fir.tex}
	\caption{Impulsantworten der implementierten Filter}
	\label{fig:fir}
\end{figure}
\begin{table}[htb]
	\centering
	\begin{tabular}{ >{\ttfamily\selectfont}l S[table-format=2.2] s}
		\hline
		\textrm{Parameter} & {Wert} \\
		\hline
		syms & 4\\
		f\_symbol & 32 & \mega\Baud\\
		n\_up & 10\\
		\hdashline
		\textrm{RC \& RRC:}\\
		r & 0.33\\
		\hdashline
		\textrm{Gauss:}\\
		energy\_factor & 0.99\\
		\hline
	\end{tabular}
	\caption{Parameter für Impulsantworten in \cref{fig:fir}}
	\label{tab:fir_params}
\end{table}

\newpage
\subsubsection{Signalgenerierung und Verstärkung}

Für die Generierung des Sendesignals (\code{generate\_signal}) und die Verstärkung des Signals (\code{amplifier}) sind in separaten Funktionen implementiert, um später auch längere Glasfasern mit zwischen geschalteten Verstärkern simulieren zu können.\\

Die Funktion \code{generate\_signal} erzeugt ein Sendesignal mit gegebener Pulsformung und Modulation. Die Sendesymbole können sowohl reell als auch komplex vorliegen. Die Zuordnung von Sendesymbolen zu Bits erfolgt über das \code{Dictionary} \code{modulation}. In diesem Datentyp werden die zur Verfügung stehenden Symbole als Werte den Sendebits als Schlüssel zugeordnet. Für die BPSK sieht das \code{Dictionary} \code{modulation} folgendermaßen aus: \code{\{'0': -1, '1': 1\}} Auch hier wird, wie bei der Berechnung der Impulsantworten, das resultierende Signal auf einen Festen Energiewert normiert. Dieser Entspricht der Anzahl der Symbole, zum Beispiel bei vier zu sendenden Symbolen beträgt die Energie $E_\text{signal}$ des Sendesignals \SI{4}{\joule}. Die Normierung wird hier auch mit \cref{eq:enormierung} vorgenommen, nur das hier der Skalierungsfaktor noch durch $\sqrt{n_\text{symbol}}$ geteilt wird. Die Wurzel hier hat ebenfalls wieder den Grund das die Energie quadratisch mit den Signalwerten zusammen hängt.\\

Die aktuelle Leistung $P_\text{is}$ wird mit \cref{eq:leistung_signal} berechnet. Der Zielwert für Leistung $P_\text{should}$ wird in als \code{P\_in} in dBm an die Funktion übergeben und innerhalb der Methode mit
\begin{equation}
P_\text{should} = 10^{\frac{P_\text{dBm}-30}{10}}
\end{equation}
in den linearen Wert umgerechnet. Der Faktor für die Verstärkung $f_\text{amp}$ auf den Leistungswert \code{P\_in} wird mit Hilfe der \cref{eq:leistung_symbol} berechnet. Von diesem Skalierungsfaktor muss auch wieder die Wurzel gezogen werden, da die Leistung quadratisch mit der Amplitude zusammen hängt
\begin{align}
f_\text{amp} = \sqrt{\frac{P_\text{should}}{P_\text{is}}} \eqdot
\end{align}\\


Die Funktion \code{add\_zeros} fügt an das Signal am Anfang und Ende weitere Nullen hinzu. Das Zeitfenster wird  vergrößert, um auch nach Dispersion auf der Faser das gesamte Signal im Zeitfenster zu finden. Ohne diese Maßnahme bewegen sich Pulse über die Grenzen des Zeitintervalls und treten am anderen Ende wieder in das Zeitfenster ein \cite{Engelbrecht2014}.\\

Zur Generierung des Sendesignals werden die in \cref{tab:parameters_generate_signal} beschriebenen Parameter benötigt.
\begin{table}[htb]
  \centering
  \begin{tabularx}{\linewidth}{ >{\ttfamily\selectfont}l >{\ttfamily\selectfont}l X }
    \hline
    \textrm{Parameter} & \textrm{Datentyp} & Beschreibung \\
    \hline
    modulation & dict & Zuordnung von Sendebits zu Sendesymbolen\\
    T\_sample & float & Dauer eines Samples der Impulsantwort\\
    T\_symbol & float & Dauer eines Symbols\\
    data & array(int) & Liste die die zu sendenden Bits enthält\\
    pulse & array(float) & Impulsantwort des Filters der für die Modulation genutzt werden soll\\
    syms & int & Normierte Anzahl der Symbole. Kann auf 0 gesetzt werden wenn der Puls nicht die erste Nyquistbedingung erfüllt. Die Pulse werden dann nicht überlagert.\\
    \hdashline
    \multicolumn{3}{l}{Optionale Parameter}\\
    \hdashline
    P\_in & float & Leistung\\
    \hline
  \end{tabularx}
  \caption{Parameter für die Funktion \code{generate\_signal}}
  \label{tab:parameters_generate_signal}
\end{table}

Die Funktionen \code{generate\_signal}, \code{amplifier} und \code{add\_zeros} geben das resultierende Signal in einem Array zurück.

\subsection{Split-Step-Fourier-Methode}

Wie schon in \cref{chap:einleitung} beschrieben existiert eine Implementierung des \ac{SSFM} in Matlab unter dem Namen \textit{SSPROP} \cite{SSPROP}. Diese dient auch als Vorlage für die Implementierung in Python. Da in der Methode eine größere Anzahl von \ac{FFT} und \ac{IFFT} Transformationen angewendet werden, ist auch eine Implementierung in \CC\ diskutiert worden. Die Untersuchung der Performance der \ac{FFT}-Bibliothek aus NumPy zeigt, wie im Folgenden dargelegt wird, dass diese für die Implementierung der \ac{SSFM} ausreichend ist. Zunächst wurde die Implementierung der \ac{SSFM} so umgesetzt, dass die internen Variablen der Funktion keine Einheiten besitzen, so können die Eingabewerte beliebige, untereinander konsistente Einheiten besitzen. Die Umrechnung von $\alpha$ vom logarithmischen in den linearen Wert muss hierbei jedoch vom Nutzer vorgenommen werden. Daher ist die Funktionsimplementierung dahingehend angepasst und setzt nun voraus, dass die Parameter mit der Länge des \ac{LWL} zusammen hängen in \si{\kilo\meter} angegeben werden müssen.\\

Die Funktion \code{splitstepfourier}, mit der die Simulation des \ac{LWL} mit
Hilfe der SSFM durchgeführt wird, akzeptiert die in \cref{tab:parameters_ssfm} beschrieben Parameter.
\begin{table}[htb]
  \centering
  \begin{tabularx}{\linewidth}{ >{\ttfamily\selectfont}l >{\ttfamily\selectfont}l X }
    \hline
    \textrm{Parameter} & \textrm{Datentyp} & Beschreibung \\
    \hline
    u0 & array(arg1) & Eingangssignal der Glasfaser\\
    dt & float & Dauer eines Samples des Eingangssignals\\
    dz & float & Schrittweite. Definition ist in \cref{sec:ssfm} beschrieben ($\delta z$) \\
    nz & int & Anzahl der Schritte\\
    alpha & float & Dämpfungsparameter\\
    beta2 & float & Dispersionsparameter\\
    gamma & float & Nichtlinearitätsparameter\\
    \hdashline
    \multicolumn{3}{l}{Optionale Parameter}\\
    \hdashline
    return\_dict & bool & Wenn auf \code{True} gesetzt, gibt die Funktion ein Dictionary mit dem Signal nach jedem Schritt zurück\\
    \hline
  \end{tabularx}
  \caption{Parameter für die Funktion \code{splitstepfourier}}
  \label{tab:parameters_ssfm}
\end{table}

Die Funktion gibt abhängig vom Wert des Parameters \code{return\_dict} verschiedene Datentypen zurück. Falls nur das Signal am Ende des Kanals ausgegeben werden soll, also der Wert von \code{return\_dict} auf \code{False} gesetzt wird, wird ein Array, dass das Signal am Ende der Glasfaser enthält zurück gegeben. Soll das Signal nach jedem Schritt abgespeichert werden muss der Wert von \code{return\_dict} auf \code{True} gesetzt werden. Der Rückgabewert ist dann ein \code{Dictionary} in dem die Schlüssel den Simulationsschritt enthalten und die zugehörigen Werte das Signal nach diesem Schritt sind. Ein \code{Dictionary} bei der Simulation mit 3 Schritten sieht beispielsweise so aus: \code{\{'0.5': [...], '1.5': [...], '2.5': [...], '3': [...]\}}.

\subsection{Tests}

Im Notebook \code{tests.ipynb} werden alle implementierten Funktionen aufgerufen und es kann visuell die korrekte Funktionsweise der implementierten Funktionen überprüft werden. Diese Tests entsprechen keinen \textit{Unit-Tests} im herkömmlichen Sinne, können aber dennoch zur Überprüfung der Implementierung genutzt werden.

\section{Simulation}\label{sec:simulation}

Die Simulationen erfüllen mehrere Zwecke. Einerseits wird mit ihnen die korrekte Funktionsweise der \ac{SSFM} überprüft, andererseits wird gleichzeitig auch die Leistungsfähigkeit des Codes evaluiert. Außerdem können mit den Simulationen geeignete Parameter für die \ac{SSFM} ermittelt werden, bei denen die benötigte Rechenleistung möglichst niedrig ist, bei der Genauigkeit der Berechnung aber dennoch keine Kompromisse eingegangen werden müssen.\\

Für die Simulationen ist folgender Ablauf festgelegt: Zunächst wird ein Filter gewählt, mit dem das Signal moduliert werden soll. Hier wird meistens, sofern nicht anders angegeben, das \ac{RC}-Filter mit den in \cref{tab:defaults_ir_rc} genannten Parametern verwendet.\\
\begin{table}[htb]
  \centering
  \begin{tabular}{ >{\ttfamily\selectfont}l S[table-format=3.2] s}
    \hline
    \textrm{Parameter} & {Wert} & {Einheit} \\
    \hline
    syms & 4\\
    r & 0.33\\
    f\_symbol & 32 & \mega\Baud\\
    n\_up & 10\\
    \hline
  \end{tabular}
  \caption{Standard-Parameter für die Filter-Funktionen}
  \label{tab:defaults_ir_rc}
\end{table}

Nach der Erzeugung der Impulsantwort des Filter wird das Signal mit einer Modulation erzeugt. Als Modulation wird in der Regel \ac{BPSK} verwendet. Die zu sendenden Modulationssymbole werden zufällig ausgewählt. In diesem Schritt wird das Signal auch auf die Leistung $P_\text{in}$ verstärkt. Die hierfür üblichen Wert sind in \cref{tab:defaults_signal} zu sehen.\\
\begin{table}[htb]
    \centering
    \begin{tabular}{ >{\ttfamily\selectfont}l S[table-format=3.3e4] s }
        \hline
        \textrm{Parameter} & {Wert} & {Einheit} \\
        \hline
        modulation & \multicolumn{2}{l}{BPSK: \code{\{'0': -1, '1': 1\}}}\\
        n\_symbol & 30\\
        P\_in & 5 & \dBm\\
        \hline
    \end{tabular}
    \caption{Standard-Parameter für die Signalgenerierung und Verstärkung}
    \label{tab:defaults_signal}
\end{table}


Sofern im Folgenden nicht anders angegeben, werden für die Simulationen die in \cref{tab:defaults_ssfm} beschriebenen Parameter für die \ac{SSFM} genutzt. Diese Werte sind aus der Literatur entnommen, beruhen auf den physikalischen Eigenschaften des \ac{LWL} wie in \cref{subsec:lwl} beschrieben oder sind im Rahmen dieser Arbeit experimentell ermittelt worden.\\
\begin{table}[htb]
  \centering
  \begin{tabular}{ >{\ttfamily\selectfont}l S[table-format=3.3e4] s l}
    \hline
    \textrm{Parameter} & {Wert} & {Einheit} & {Quelle} \\
    \hline
    z\_length & 70 & \km & \cref{chap:einleitung}\\
    dz ($\Delta z$) & 7 & \kilo\m & \cref{subsec:stepsize_result} \\
    nz & 10 & & \cref{subsec:stepsize_result}\\
    alpha $(\alpha)$ & 0.2 & \decibel\per\kilo\metre & \cref{subsec:lwl}/\cite{g652} \\
    beta2 $(\beta_2)$ & -2.167e-23 & \s^2\per\kilo\m & \cref{subsec:lwl}/\cite{g652} \\
    gamma $(\gamma)$ & 1.3 & \per\watt\per\kilo\metre & \cref{subsec:lwl}/\cite{g652} \\
  \hline
  \end{tabular}
  \caption{Standard-Parameter für die \acl{SSFM}}
  \label{tab:defaults_ssfm}
\end{table}


\subsection{Vergleich mit SSPROP}\label{subsec:compare_ssprop}

Zunächst wird überprüft ob die \ac{SSFM}-Implementierung in Python die selben Ergebnisse wie die SSPROP-Implementierung liefert. Hierfür wird ein Signal in Python generiert und dieses sowohl in Python als auch in Matlab in die \ac{SSFM} Methode als Eingabewert übergeben. Außerdem wird die Ausführungsdauer der \code{splitstepfourier}-Funktion in Python und der SSPROP-Funktion in Matlab gestopt, damit über den Rechenzeitunterschied eine Aussage getroffen werden kann. Die Ergebnisse aus Matlab werden zur Darstellung wieder in Python importiert.

\subsection{Visualisierung}\label{subsec:propagation_visualisation}

Zum besseren Verständnis der Signalverzerrung auf einem \ac{LWL} wird das Signal nach jedem Schritt $\Delta z$ in einen neuen Graphen dargestellt. Die Graphen werden übereinander angeordnet, so kann die Verzerrung des Signals gut nachvollzogen werden. Zur besseren Anschaulichkeit wird hier $\alpha = 0$ gesetzt damit die Skalierung der einzelnen Graphen gleich bleibt.

\subsection{Schrittweite}\label{subsec:stepsize}

Zunächst soll eine geeignete Schrittweite ermittelt werden. Hierfür wird die Simulation mit Eingangsleistungen ($P_{\text{in}}$) von \SIrange{-5}{9}{\dBm} durchgeführt. Für jede Eingangsleistung wird eine Simulation mit Schrittanzahlen ($nz$) von \numrange{1}{100} durchgeführt. Das Testsignal besteht aus 1000 Symbolen. Für jedes Ausgangssignal wird der relative Fehler zum Referenzsignal berechnet. Als Referenzsignal wird das Signal mit 100 Schritten gewählt. Im Graphen werden dann die Anzahl der Schritte auf der x-Achse und der relative Fehler auf der y-Achse für jede Eingangsleistung aufgezeichnet.\\

Diese gesamte Simulation wird mit den Modulationsarten \ac{BPSK}, \textit{\ac{QPSK}} und \textit{\ac{16QAM}} durchgeführt.

\subsection{Überabtastung}\label{subsec:oversampling}

Um den Einfluss der Überabtastung auf die Qualität des Empfangssignals zu überprüfen wird das selbe Signal mit den Überabtastungsfaktoren $n_\text{up} =$ \numlist{1;2;3;4;10;16} übertragen und die Signale im gleichen Graphen aufgezeichnet.

\subsection{Übertragung über lange Distanzen}\label{subsec:long_distance}

Als letzte Simulation wird eine Langstrecken-Übertragung simuliert. Es werden mehrere \SI{70}{\km} \ac{LWL} hintereinander geschaltet. Zwischen die \ac{LWL} wird jeweils ein Verstärker geschaltet, der das Signal auf eine gegebene Eingangsleistung verstärkt. Hierfür wird die Funktion \code{amplifier} mit den Parametern der Simulation aufgerufen um das Signal wieder auf die selbe Leistung wie zu Beginn der Übertragung zu verstärken. Zur Überprüfung der Implementierung wird die Signalleistung über der gesamten \ac{LWL}-Strecke aufgezeichnet. Zum Visualisieren der Verzerrung auf dem \ac{LWL} wird das Signal im Zeit- und im Frequenzbereich am Anfang und am Ende der gesamten Strecke geplottet. Diese Simulation entspricht der physischen Ausführung eines Glasfaserlinks \cite{Grau1991}. Die Verstärker sind nötig um am Empfänger ein ausreichendes \textit{\ac{SNR}} garantieren, da ansonsten das Signal aufgrund der Dämpfung eine zu kleine Amplitude hat. \cite{Grau1991} Die hier verwendeten Verstärker werden als ideal und rauschfrei implementiert.