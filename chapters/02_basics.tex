\chapter{Grundlagen}\label{chap:theorie}

\section{Mathematische Grundlagen}
\subsection{Lösungen einfacher Differentialgleichungen}

In dieser Arbeit treten gewöhnliche lineare Differentialgleichungen in der Form
\begin{equation}
    \frac{\partial f(x)}{\partial x} = c\cdot f(x)
    \label{eq:dgl}
\end{equation}
auf. Die Lösung für diese Differentialgleichung ist nach \cite{Bronstein2016} 
\begin{equation}
    f(x) = \exp(c\cdot x)f(0) \eqdot
    \label{eq:dgl_solution}
\end{equation}

\subsection{Fehlerberechnung}
Der Vergleich von Signalen mit Referenzsignalen erfolgt in dieser Arbeit anhand des relativen Fehlers. Für diskrete Signale $x = \left[x_1,x_2,...,x_i\right]$ ist der relative Fehler $\delta_x$ folgendermaßen definiert:

\begin{equation}
    \delta_x = \frac{\sum_{i=1}^N |x_i-x_{ref,i}|^2}{\sum_{i=1}^N|x_{ref,i}|^2} \eqdot
\end{equation}

\subsection{Signalverarbeitung}

\subsubsection{Shannon-Nyquist-Abtasttheorem}

Das Shannon-Nyquist-Theorem beschreibt den maximalen Zeitabstand $T_{\text{sample}} < 1/(2f_{\text{max}})$ zwischen zwei Abtastwerten, der notwendig ist, um eine kontinuierliche Funktion mit der maximalen Frequenz $f_{\text{max}}$ eindeutig aus Abtastwerten  zu rekonstruieren \cite{Kammeyer2018}. Im Folgenden wird der Überabtastungsfaktor mit $n_\text{up}$ abgekürzt. Er wird aus dem Verhältnis von Symboldauer $T_\text{symbol}$ und Abtastrate $T_\text{sample}$ berechnet:
\begin{equation}
    n_\text{up} = \frac{T_\text{symbol}}{T_\text{sample}} \eqdot
\end{equation}

\subsubsection{Energiesignal}\label{subsec:energysignal}

Ein Energiesignal ist nach \cite{Puente2019} als Signal mit endlicher Energie definiert. Für diese Signale gilt
\begin{equation}
    E = \int_{-\infty}^\infty g(t)g^*(t) \mathrm{d}t = \int_{-\infty}^\infty |g(t)|^2\mathrm{d}t < \infty \eqdot
    \label{eq:energy_signal_cont}
\end{equation}
Für den diskreten Fall mit Abtastzeit $T_{\textrm{sample}}$ lässt sich das (Riemann-) Integral in eine Riemann-Summe umschreiben
\begin{equation}
    E \approx \sum_{k=-\infty}^\infty |g(kT_{\textrm{sample}})|^2 T_{\textrm{sample}} = T_{\textrm{sample}}\sum_{k=-\infty}^\infty \cdot |g(kT_{\text{sample}})|^2 < \infty
    \label{eq:energy_signal_disc}
\end{equation}
Die Leistung dieses Signals ergibt sich aus der aufgewendeten Energie $\Delta E$ geteilt durch die dabei verstrichene Zeit $\Delta t$:
\begin{equation}
    P = \frac{\Delta E}{\Delta t}
\end{equation}
Damit ergibt sich die Leistung pro Symbol, bei einer Symboldauer von $T_\text{symbol}$ zu:
\begin{equation}
    P_{\textrm{symbol}} = \frac{E_{\textrm{symbol}}}{T_{\textrm{symbol}}} = \frac{T_{\textrm{sample}}}{T_{\textrm{symbol}}} \cdot \sum_{k=-\infty}^\infty |g(kT_{\text{sample}})|^2 = \frac{1}{n_\text{up}} \cdot \sum_{k=-\infty}^\infty |g(kT_{\text{sample}})|^2 \eqdot
    \label{eq:leistung_symbol}
\end{equation}
Für ein Signal mit $n_\text{symbol}$ Symbolen ist die Leistung damit
\begin{equation}
P_\text{signal} = \frac{E_\text{signal}}{T_\text{signal}} = \frac{E_\text{signal}}{T_\text{symbol} \cdot n_\text{symbol}} = \frac{1}{n_\text{up} \cdot n_\text{symbol}} \cdot \sum_{k=-\infty}^\infty |g(kT_{\text{sample}})|^2 \eqdot
\label{eq:leistung_signal}
\end{equation}

\subsubsection{FIR Filter}

\textit{\ac{FIR}} Filter zeichnet laut Definition eine endlich lange Impulsantwort aus. Somit kann der Filter mit einer endlichen Anzahl von Beobachtungszeitschritten komplett charakterisiert werden und eignet sich sehr gut für eine digitale Implementierung. Aufgrund der zeitlichen Begrenzung handelt es sich bei der Impulsantwort jedes FIR Filters um ein Energiesignal \cite{Puente2019}. In dieser Arbeit kommen FIR Systeme zur Generierung der unterschiedlichen Pulsformen und Systemantworten zum Einsatz.

\subsubsection{Erste Nyquistbedingung}
Bei bandbegrenzten Kanälen muss \ac{ISI} vermieden werden \cite{Kammeyer2018}. Die erste Nyquistbedingung muss erfüllt werden, damit eine ISI-freie Übertragung gewährleistet werdenn kann. Sie besagt, dass die Impulsantwort $h(t)$ des Gesamtübertragungssystems mit der Abtastrate $T_\text{symbol}$ zu den Abtastzeitpunkten $n T_\text{symbol},\ n \in\mathbb{Z}$ folgende Bedingung erfüllen muss
\begin{equation}
h(n T_\text{symbol}) =
    \begin{cases}
    1 & n = 0\\
    0 & n \neq 0
    \end{cases}
\eqdot
\end{equation}

\section{Physikalische Grundlagen}
\subsection{Lichtwellenleiter}\label{subsec:lwl}

Es existieren viele verschiedene \ac{LWL}-Arten. Hier wird nur auf die zwei am weitesten verbreiteten Arten eingegangen: Mono- und Multimode-\ac{LWL}. Der schematische Aufbau eines \ac{LWL} ist in \cref{fig:lwl} zu sehen. Der Kern (\textit{Core}) und der Mantel (\textit{Cladding}) bestehen beide aus einem optisch leitenden Medium, allerdings mit unterschiedlichen Brechungsindizes. Die Funktion des Lichtwellenleiters resultiert aus einer Änderung der Brechungsindex von \textit{Core} zu \textit{Cladding}. Das Licht kann aufgrund von Wellenführung sich nicht außerhalb des \textit{Core} ausbreiten: Im \textit{Cladding} fallen die Feldkomponenten exponentiell ab. Um den Mantel befindet sich eine Schutzbeschichtung (\textit{Coating}), die den \ac{LWL} vor mechanischen Schäden schützt. Die äußere Hülle (\textit{Jacket}) besteht meist aus Kunststoff oder Metall um den \ac{LWL} vor äußeren Einflüssen zu schützen. Kabel, die in der Erde verlegt werden, haben meist noch in der äußeren Hülle Metalldrähte zur Stabilisierung sowie einen Schutz vor Tierbissen. Bei Seekabeln kommen mehrschichtige Verkleidungen zum Einsatz, die vor Feuchtigkeit und Salz versiegeln, sowie die mechanische Beanspruchung beim Verlegen der Kabel ausgleichen \cite{Engelbrecht2014,Agrawal2013,Grau1991,Mitschke2016}. 

Der Hauptunterschied zwischen Mono- und Multimode-\ac{LWL} liegt im Kerndurchmesser. Wenn der Kerndurchmesser klein genug ist, dass sich nur die Grundmode ausbreiten kann, wird von einem Monomode-\ac{LWL} gesprochen.. Grundsätzlich werden über lange Distanzen lediglich Monomode-\ac{LWL} verwendet, da Multimode-Fasern mit zunehmender Länge stärkere Signalverfälschungen aufgrund der Modendispersion zeigen.

\subsubsection{Aufbau}

In dieser Arbeit werden Monomode-\ac{LWL} mit einer Wellenlänge von \SI{1550}{\nano\m}, also einem \textit{Core}-Durchmesser von \SI{9}{\micro\m} und einem \textit{Cladding}-Durchmesser von \SI{125}{\micro\m} betrachtet. Diese haben ein \textit{Coating} mit einem Durchmesser von \SI{245}{\micro\m}.

\begin{figure}[htb]
    \centering
    \input{figures/tikz/lwl_scheme.tex}
    \caption{Aufbau \acl{LWL}, nicht maßstabsgetreu}
    \label{fig:lwl}
\end{figure}

\subsubsection{Parameter}

Für die Simulation der \ac{LWL} wird der \textit{\ac{GVD}}-Parameter benötigt. Dieser ist bei \ac{LWL} nicht direkt im Datenblatt angegeben. Dort wird nur der maximale chromatische Dispersionskoeffizient $D$ angeführt, der in der Einheit \si{\pico\s\per\nano\m\per\kilo\m} angegeben ist. Für die in dieser Arbeit simulierten \ac{LWL} wird $D = \SI{17}{\pico\s\per\nano\m\per\kilo\m}$ angenommen. Dieser Wert wird mit \cref{eq:beta2} in den $\beta_2$ Parameter umgerechnet.
\begin{equation}
	\beta_2 = -\frac{D \cdot \lambda^2}{(2 \pi\cdot c_0)} = -\frac{17 \cdot (\SI{1550}{\nano\m})^2}{2\pi\cdot \SI{3e8}{\m\per\s}} = - \SI{2,167e-20}{\s^2\per\m} = -\SI{2.167e-23}{\s^2\per\kilo\m}
	\label{eq:beta2}
\end{equation}

\subsection{Nichtlineare Schrödingergleichung}\label{subsec:nlsg}

Die \ac{NLSG}
\begin{equation}
\frac{\partial u(t,z)}{\partial z}+ \beta_1\frac{\partial u(t,z)}{\partial t}+j\frac{\beta_2}{2}\frac{\partial^2 u(t,z)}{\partial t^2}+\frac{\alpha}{2}u(t,z)=j\gamma(\omega_0)|u(t,z)|^2u(t,z)
\label{eq:nlse_complete}
\end{equation}
kann wie in \cite{Agrawal2013} beschrieben aus den Maxwell-Gleichungen hergeleitet werden. Sie beschreibt die komplexe Einhüllende $u$ eines Signals $s$ in einem \ac{LWL}, wie in \cref{fig:signal} dargestellt, in Abhängigkeit von der Zeit $t$ und dem Ort $z$ und wird in dieser Arbeit allgemein als "`das betrachtete Signal"' referenziert. Die Bezeichnung der \ac{NLSG} ist auf ihre mathematische Struktur zurückzuführen, die ähnlich zur Schrödingergleichung ist.\\

Im Folgenden werden Signale im Zeitbereich mit kleinen Buchstaben repräsentiert und Signale im Frequenzbereich mit großen Buchstaben dargestellt.\\

\begin{figure}[htb]
	\centering
	\input{figures/signal.tex}
	\caption{Darstellung Signal $s$ und komplexe Einhüllende $u$.}
	\label{fig:signal}
\end{figure}

Die Parameter $\beta_1$ und $\beta_2$ sind die Dispersionskoeffizenten. $\beta_1$ wird in dieser Arbeit vernachlässigt, da dieser Parameter die Gruppenlaufzeit repräsentiert und daher nur als Verzögerung des Signals im monochromatischen \ac{LWL} wirkt. Dadurch verschwindet der Term $\beta_1\frac{\partial u(t,z)}{\partial t}$ und \cref{eq:nlse_complete} ändert sich zu 
\begin{equation}
\frac{\partial u(t,z)}{\partial z}+j\frac{\beta_2}{2}\frac{\partial^2 u(t,z)}{\partial t^2}+\frac{\alpha}{2}u(t,z)=j\gamma(\omega_0)|u(t,z)|^2u(t,z) \eqcomma
\label{eq:nlse}    
\end{equation}
was als Basis für die weiteren Betrachtungen dient. $\beta_2$ wird als \textit{\ac{GVD}} Koeffizient bezeichnet, dieser Parameter ist dafür verantwortlich, dass das Signal in dem \ac{LWL} im Zeitbereich "`breiter"' wird. Der Parameter $\gamma$ repräsentiert die Nichtlinearität des \ac{LWL}. Diese bewirkt die Phasenänderung des Signals und damit einhergehend eine Verbreiterung des Spektrums. Da dieser Parameter mit dem Betragsquadrat des Signals multipliziert wird erhöht sich der Einfluss der Nichtlinearität bei höheren Signalamplituden. Der bei \textit{Single-Mode}-\ac{LWL} übliche Wert für $\gamma$ bei einer Lichtwellenlänge von \SI{1550}{\nano\m} ist \SI{1.3}{\per\watt\per\kilo\m}. Der Parameter $\alpha$ ist für die Dämpfung des Signals verantwortlich. Der übliche Wert für $\alpha$ beträgt \SI{0.2}{\decibel\per\kilo\m}. Daher eignet sich dieser \ac{LWL}-Typ für längere Übertragungsstrecken (ab circa \SI{2}{\km}) besonders gut, da er eine sehr niedrige Dämpfung pro Kilometer aufweist.

Die \ac{NLSG} ist für $\beta_2 \neq 0$ und $\gamma \neq 0$ für den allgemeinen Fall nicht analytisch lösbar \cite{Agrawal2013}. Für das Verständnis der \ac{SSFM} lohnt sich eine getrennte Betrachtung des linearen und nichtlinearen Teils der \ac{NLSG}. Für $\alpha \neq 0$ und $\beta_2 = \gamma = 0$ ergibt sich  \cref{eq:nlse} zu dem Dämpfungsanteil der \ac{NLSG}:
\begin{equation}
    \frac{\partial u(t,z)}{\partial z} = -\frac{\alpha}{2}u(t,z) \eqdot
	\label{eq:nlse_attenuation}
\end{equation}
Die Lösung hierfür ist mit Hilfe von \cref{eq:dgl_solution} berechnet:
\begin{equation}
    u(t,z) = \exp(-\frac{\alpha}{2} \cdot z)u(t,0) \eqdot
    \label{eq:nlse_attenuation_solved}
\end{equation}\\
Im Folgenden werden der lineare und der nichtlineare Teil der \ac{NLSG} getrennt betrachtet.

\subsubsection{Abwesenheit von Nichtlinearitäten}
Wird $\gamma = 0$ und $\alpha = 0$ gesetzt, ergibt sich die \ac{NLSG} in vereinfachter Form ohne nichtlineare Einflüsse: 
\begin{equation}
    \frac{\partial u(t,z)}{\partial z} = -j\frac{\beta_2}{2}\frac{\partial^2 u(t,z)}{\partial t^2}
	\label{eq:nlse_lin}
\end{equation}
Diese Gleichung lässt sich im Frequenzbereich analytisch lösen. Die Fouriertransformation wird mithilfe der Korrespondenz $\frac{\partial^n}{\partial t^n}x(t)\ \laplace\ (j2\pi f)^n X(f)$ durchgeführt. Die \cref{eq:nlse_lin} ergibt sich damit zu
\begin{alignat}{3}
    \frac{\partial U(f,z)}{\partial z } &= -j\frac{\beta_2}{2}\left(j2\pi f\right)^2 & &\cdot U(f,z)\\
    &= j\beta_2 2\pi^2 f^2 & &\cdot U(f,z)
    \label{eq:nlse_lin_freq}
\end{alignat}
im Frequenzbereich.

Die Lösung für diese Gleichung ist dann mit \cref{eq:dgl_solution} lösbar und ergibt sich zu
\begin{alignat}{3}
    U(f,z) &= \exp\left(j2\beta_2 \pi^2 f^2 z\right) & &\cdot U(f,0)\\
    U(\omega,z) &= \exp\left(j\frac{\beta_2}{2}\omega^2 z\right) & &\cdot U(\omega,0)
    \label{eq:nlse_lin_freq_solved}
\end{alignat}
im Frequenzbereich.

\subsubsection{Abwesenheit von Dispersion}

Wenn $\beta_2 = 0$ und $\alpha = 0$ gesetzt wird ergibt sich die \ac{NLSG} zu 
\begin{equation}
    \frac{\partial u(t,z)}{\partial z} = j\gamma(\omega_0)\left|u(t,z)\right|^2 \cdot u(t,z)
	\label{eq:nlse_nonlin}
\end{equation}
Unter der Annahme das $|u(t,z)|^2$ für die Schrittweite $\Delta z$ konstant ist kommt als Lösung
\begin{equation}
    u(t,z) = \exp\left(j\gamma(\omega_0)\left|u(t,0)\right|^2\right) \cdot u(t,0)
    \label{eq:nlse_nonlin_solved}
\end{equation}
heraus.

\subsubsection{Numerische Lösungsansätze}

Es existieren mehrere Ansätze zur Lösung der \ac{NLSG}. Neben der hier angewendeten \ac{SSFM} existiert noch die Finite-Differenzen-Methode. Diese kommt vor allem dann zum Einsatz, wenn die Berechnung der \ac{SSFM} zu aufwendig wird, was insbesondere bei der Berechnung von \ac{WDM} \cite{Agrawal2013} der Fall ist. In dieser Arbeit werden lediglich Fasern mit nur einer Wellenlänge betrachtet. Daher wird auf diese Methode nicht weiter eingegangen.

\section{Split-Step-Fourier-Methode}\label{sec:ssfm}

Wie in \cref{subsec:nlsg} erwähnt lässt sich die \ac{NLSG} im Allgemeinen Fall nicht analytisch lösen wenn $\beta_2$ und $\gamma$ ungleich $0$ sind. Als numerischer Lösungsalgorithmus hat sich hier der \ac{SSFM} etabliert. Im Folgenden wird die Methode angelehnt an \cite{Agrawal2013,Engelbrecht2014} vorgestellt. Bei dieser Methode wird der \ac{LWL} in gleichgroße Segmente unterteilt. Für diese wird nacheinander der lineare Anteil der \ac{NLSG} im Frequenzbereich und der nichtlineare Anteil im Zeitbereich berechnet. Die Umwandlung zwischen Zeit- und Frequenzbereich geschieht mit Hilfe der \textit{\ac{FFT}} und \textit{\ac{IFFT}}, die im Folgenden mit den Operatoren $\mathcal{F}$ und $\mathcal{F}^{-1}$ dargestellt werden. Das Ausgangssignal des Segments des \ac{LWL} wird als Eingangssignal für das nächste Segment genutzt. Die getrennte Berechnung von linearem und nichtlinearem Effekt auf dem \ac{LWL} geschieht unter der Annahme, dass die Effekte für kleine Abschnittslängen unabhängig und vertauschbar sind. Die \ac{NLSG} wird dafür in die Form 
\begin{equation}
    \frac{\partial u(t,z)}{\partial z} = (\mathcal{L} + \mathcal{N}) u(t,z)
\end{equation}
umgeschrieben.\\
Der nichtlineare Anteil wird durch den Operator $\mathcal{N}$ und der lineare Anteil durch den Operator $\mathcal{L}$ repräsentiert. Im linearen Operator sind außerdem noch die Verluste durch die Dämpfung des \ac{LWL} enthalten. Die Operatoren ergeben sich aus \cref{eq:nlse} zu
\begin{align}
    \mathcal{L} &= -j\frac{\beta_2}{2}\frac{\partial^2 u(t,z)}{\partial t^2}-\frac{\alpha}{2}\\
    \mathcal{N} &= j\gamma(\omega_0)|u(t,z)|^2 \eqdot
\end{align}

In \cref{fig:ssfm} ist die Funktionsweise des \ac{SSFM} visualisiert. Diese Implementierung verursacht einen Fehler $\mathcal{O}(\Delta z^2)$ zweiter Ordnung. Damit muss eine sehr kleine Schrittweite $\Delta z$ gewählt werden um ein ausreichend genaues Ergebnis zu bekommen.

\begin{figure}[htb]
    \centering
    \input{figures/tikz/ssfm.tex}
    \caption{\acl{SSFM}}
    \label{fig:ssfm}
\end{figure} 

\subsection{Symmetrische Split-Step-Fourier-Methode}\label{subsec:ssfm_sym}

Die Fehlerordnung kann auf $\mathcal{O}(\Delta z)$ reduziert werden indem der nichtlineare Schritt in die Mitte des Fasersegments gelegt wird \cite{Engelbrecht2014}. Der Ablauf der symmetrischen \ac{SSFM} ist in \cref{fig:ssfm_sym} dargestellt. Hierdurch verdoppelt sich die Anzahl der \ac{FFT}- und \ac{IFFT}-Operationen, dies kann sich aber dadurch kompensiert werden, dass die Schrittweite $\Delta z$ vergrößert werden kann, und somit weniger Schritte pro \ac{LWL} notwendig sind.

\begin{figure}[htb]
    \centering
    \input{figures/tikz/ssfm_sym.tex}
    \caption{Symmetrische \acl{SSFM}}
    \label{fig:ssfm_sym}
\end{figure}

Der Rechenaufwand durch die \ac{FFT}- und \ac{IFFT}-Operationen kann reduziert werden indem zwei Halbschritte immer zu einem ganzen Schritt zusammen gefasst werden. Lediglich am Anfang und am Ende des \ac{LWL} werden dann ein Halbschritt benötigt. Die Reduzierung hat zur Folge das die Einhüllende $u(t,z)$ nicht mehr zur Berechnung des Nichtlinearen Operators $\mathcal{L}$ zur Verfügung steht. Daher wird stattdessen der Wert $u(t,z+\frac{\Delta z}{2})$ für die Berechnung benutzt. In \cite{Plura2004} wird gezeigt, dass diese Reduzierung bei speziellen Testsignalen (Solitonen) zur Verschlechterung der Approximation führt. In den meisten Fällen ist das reduzierte Verfahren dennoch vorzuziehen, da die Einsparung an Rechenaufwand den Verlust an Genauigkeit aufwiegt. Das reduzierte Verfahren ist in \cref{fig:ssfm_sym_red} zu sehen.

\begin{figure}[htb]
    \centering
    \input{figures/tikz/ssfm_sym_red.tex}
    \caption{Reduzierte symmetrische \acl{SSFM}}
    \label{fig:ssfm_sym_red}
\end{figure}